import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChatListComponent } from './chat-list/chat-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResultComponent } from './result/result.component';
import { OnboardingComponent } from './onboarding/onboarding.component';
import { GaugeModule } from 'angular-gauge';
import { PricingComponent } from './pricing/pricing.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { FinalComponent } from './final/final.component';
@NgModule({
  declarations: [
    AppComponent,
    ChatListComponent,
    LoginComponent,
    RegisterComponent,
    ResultComponent,
    OnboardingComponent,
    PricingComponent,
    SidebarComponent,
    CheckoutComponent,
    FinalComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        NgbModule,
        ReactiveFormsModule,
        HammerModule,
        BrowserAnimationsModule,
        GaugeModule.forRoot(),
        FontAwesomeModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
