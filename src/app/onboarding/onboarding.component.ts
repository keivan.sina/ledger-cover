import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
export class OnboardingComponent implements OnInit {
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  currentSlide = 1;
  // constant for swipe action: left or right
  SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };

  // action triggered when user swipes
  swipe(currentIndex: number, action = this.SWIPE_ACTION.RIGHT) {
      // swipe right, next avatar
      if (action === this.SWIPE_ACTION.RIGHT && this.currentSlide > 1) {
        this.currentSlide -= 1;
      }

      // swipe left, previous avatar
      if (action === this.SWIPE_ACTION.LEFT  && this.currentSlide < 3) {
        this.currentSlide += 1;
      }
      console.log('swipe:' + this.currentSlide);
  }
  constructor(
    public router: Router
  ) {}

  ngOnInit(): void {
    if (localStorage.getItem('user')) {
      this.router.navigate(['/chat']);
    }
  }

}
