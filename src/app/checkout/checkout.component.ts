import { Router } from '@angular/router';
import { PricingItem } from './../pricing-item.model';
import { DataService } from './../data.service';
import { Component, OnInit } from '@angular/core';
import { faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';
import { Pricing } from './../pricing.model';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  faTimes = faTimes;
  faCheck = faCheck;
  plan;
  option1 = false;
  option2 = false;
  option3 = false;
  option4 = false;
  constructor(
    private data: DataService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.data.currentPlan.subscribe(currentPlan => this.plan = currentPlan);
  }

  toggleVisibility(e){
    this.option1 = e.target.checked;
  }
}
