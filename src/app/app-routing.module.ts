import { PricingComponent } from './pricing/pricing.component';
import { FinalComponent } from './final/final.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ChatListComponent} from './chat-list/chat-list.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {ResultComponent} from './result/result.component';
import {OnboardingComponent} from './onboarding/onboarding.component';
import { from } from 'rxjs';


const routes: Routes = [
  {path: 'onboarding', component: OnboardingComponent},
  {path: 'chat', component: ChatListComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'result', component: ResultComponent},
  {path: 'pricing', component: PricingComponent},
  {path: 'checkout', component: CheckoutComponent},
  {path: 'final', component: FinalComponent},
  { path: '',   redirectTo: '/onboarding', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
