import { PricingItem } from './../pricing-item.model';
import { DataService } from './../data.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { faTimes, faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-pricing',
  templateUrl: './pricing.component.html',
  styleUrls: ['./pricing.component.scss']
})
export class PricingComponent implements OnInit {
  faTimes = faTimes;
  faCheck = faCheck;
  plans;
  currentPlan;
  constructor(
    public router: Router,
    private data: DataService
  ) { }

  ngOnInit(): void {
    this.data.plans.subscribe(plans => this.plans = plans);
    this.data.currentPlan.subscribe(currentPlan => this.currentPlan = currentPlan);
  }
  choosePlan(plan: PricingItem) {
    this.data.choosePlan(plan);
    this.router.navigate(['/checkout']);
  }
}
