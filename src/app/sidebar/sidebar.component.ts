import { Component, OnInit } from '@angular/core';
import { faBars } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuActive = false;
  faBars = faBars;
  constructor() { }

  ngOnInit(): void {
  }

  openMenu() {
    console.log('hi');
    this.menuActive = true;
  }
  closeMenu() {
    this.menuActive = false;
  }
}
