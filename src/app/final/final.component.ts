import { Router } from '@angular/router';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.scss']
})
export class FinalComponent implements OnInit {
  refrence: number;
  faCheck = faCheck;
  constructor(
    public router: Router,
  ) { }

  ngOnInit(): void {
    this.refrence = Math.round(Math.random() * 100000);
  }

}
