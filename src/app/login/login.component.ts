import { Component, OnInit } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {DataService} from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm;
  users;
  name: string;
  errorMessage = '';
  constructor(
    private formBuilder: FormBuilder,
    private data: DataService,
    public router: Router,
  ) {
    this.loginForm = this.formBuilder.group({
        email: '',
        password: ''
    });
  }

  ngOnInit(): void {
    this.data.currentName.subscribe(name => this.name = name);
    this.users = JSON.parse(localStorage.getItem('users'));
  }

  onSubmit(value) {
    if ((value.email === 'a@b.com' && value.password === '1') || this.check(value)) {
      this.data.changeName(value.email);
      localStorage.setItem('user', JSON.stringify(value));
      if (!localStorage.getItem('users')) {
        localStorage.setItem('users', JSON.stringify([{name: 'A', email: 'a@b.com', password: '1' }]));
      }
      this.router.navigate(['chat']);
    } else {
      this.errorMessage = 'Email or password is wrong.';
    }
  }

  check(value) {
    if (this.users.find(x => x.email === value.email)) {
      return this.users.find(x => x.email === value.email).password === value.password;
    } else {
      return false;
    }
  }
}
