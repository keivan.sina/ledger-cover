import {State} from './state.model';

export class StateMap {
  public items: Array<State>;
  constructor(items: Array<State>) {
    this.items = items;
  }
  public setItems(items: Array<State>) {
    this.items = items;
  }
}
