export class PricingItem {
    public title: string;
    public maximumCoverage: number;
    public available: boolean;
    constructor(
        title: string,
        maximumCoverage: number,
        available: boolean
    ){
        this.title = title;
        this.maximumCoverage = maximumCoverage;
        this.available = available;
    }
}
