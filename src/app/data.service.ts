import { PricingItem } from './pricing-item.model';
import { Pricing } from './pricing.model';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {StateMap} from './state-map.model';
import {State} from './state.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private messageSource = new BehaviorSubject<string>('default message');
  private currentPlanId = new BehaviorSubject<PricingItem>(null);
  private stateSource = new BehaviorSubject<StateMap>(
    new StateMap([
      new State('hello', 'Hello, How can I help you?', 'button', ['Start', 'Logout'], ''),
      new State('url', 'Enter your website URL', 'text-website', [], ''),
      new State('reurl', 'Please enter a valid website URL', 'text-website', [], ''),
      new State('sql', 'Have you had penetration test?', 'button', ['Yes', 'No'], ''),
      new State('sqlresult', 'What was the penetration test result?', 'text-number', [], ''),
      new State('result', 'Ready to see the result?', 'button', ['Yes', 'No, Start Over'], ''),
    ])
  );
  private pricingList = new BehaviorSubject<Array<Pricing>>(
    [
      new Pricing(1, 'Basic Plan', 55, [
        new PricingItem('Data Loss', 25000, true),
        new PricingItem('Privacy Liability', 0, false),
        new PricingItem('Network Liability', 0, false),
        new PricingItem('Cyber Extortion', 0, false),
        new PricingItem('Notification Cost', 0, false),
      ]),
      new Pricing(2, 'Optimal Plan', 65, [
        new PricingItem('Data Loss', 50000, true),
        new PricingItem('Privacy Liability', 25000, true),
        new PricingItem('Network Liability', 25000, true),
        new PricingItem('Cyber Extortion', 0, false),
        new PricingItem('Notification Cost', 10000, true),
      ]),
      new Pricing(3, 'Premium Plan', 75, [
        new PricingItem('Data Loss', 75000, true),
        new PricingItem('Privacy Liability', 50000, true),
        new PricingItem('Network Liability', 50000, true),
        new PricingItem('Cyber Extortion', 25000, true),
        new PricingItem('Notification Cost', 10000, true),
      ]),
      new Pricing(4, 'Custom Plan', 99, [
        new PricingItem('Data Loss', -1, true),
        new PricingItem('Privacy Liability', -1, true),
        new PricingItem('Network Liability', -1, true),
        new PricingItem('Cyber Extortion', -1, true),
        new PricingItem('Notification Cost', -1, true),
      ]),
    ]
  );
  currentName = this.messageSource.asObservable();
  stateMap = this.stateSource.asObservable();
  plans = this.pricingList.asObservable();
  currentPlan = this.currentPlanId.asObservable();
  constructor() { }

  changeName(name: string) {
    this.messageSource.next(name);
  }
  changeStateMap(stateMap: StateMap) {
    this.stateSource.next(stateMap);
  }
  choosePlan(plan: PricingItem) {
    this.currentPlanId.next(plan);
  }
}
