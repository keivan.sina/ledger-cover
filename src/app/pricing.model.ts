import { PricingItem } from './pricing-item.model';

export class Pricing {
    public id: number;
    public name: string;
    public price: number;
    public items: Array<PricingItem>;
    constructor(
        id: number,
        name: string,
        price: number,
        items: Array<PricingItem>
    ) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.items = items;
    }
}
