import { Router } from '@angular/router';
import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {DataService} from '../data.service';
import {StateMap} from '../state-map.model';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss'],
  providers: [NgbRatingConfig]
})
export class ChatListComponent implements OnInit, AfterViewChecked {
  chats = [
    { sender: 0, message: 'Hello, How can I help you?' }
  ];
  stateMap: StateMap;
  state = 'hello';
  name = 'John';
  textForm;
  processing = false;
  counter = 0;
  typing = false;
  @ViewChild('scrollframe', {static: false}) scrollFrame: ElementRef;
  private scrollContainer: any;
  constructor(
    private formBuilder: FormBuilder,
    private data: DataService,
    private rateConfig: NgbRatingConfig,
    public router: Router
  ) {
    rateConfig.max = 5;
    rateConfig.readonly = true;
    this.textForm = this.formBuilder.group({
      answer: ''
    });
  }

  ngOnInit(): void {
    this.data.currentName.subscribe(name => this.name = name);
    this.data.stateMap.subscribe(stateMap => this.stateMap = stateMap);
    console.log(JSON.parse(localStorage.getItem('users')));
  }
  ngAfterViewChecked() {
    this.scrollContainer = this.scrollFrame.nativeElement;
  }

  scrollToBottom(): void {
    this.scrollContainer.scroll({
      top: this.scrollContainer.scrollHeight,
      left: 0,
      behavior: 'smooth'
    });
  }
  currentState() {
    return {
      object: this.stateMap.items.filter((st) => st.status === this.state)[0],
      index: this.stateMap.items.findIndex((st) => st.status === this.state),
    };
  }
  stateMachine(answer: string) {
    this.chats.push({sender: 1, message: answer});
    switch (this.currentState().object.status) {
      case 'hello':
        if (answer === 'Logout') {
          localStorage.removeItem('user');
          this.router.navigate(['/login']);
        } else {
          this.changeState('url', answer);
        }
        break;
      case 'url':
        if (this.websiteValidator(answer)) {
          this.changeState('sql', answer);
        } else {
          this.changeState('reurl', answer);
        }
        break;
      case 'reurl':
        if (this.websiteValidator(answer)) {
          this.changeState('sql', answer);
        } else {
          this.changeState('reurl', answer);
        }
        break;
      case 'sql':
        if (answer === 'Yes') {
          this.changeState('sqlresult', answer);
        } else {
          this.changeState('result', answer);
        }
        break;
      case 'sqlresult':
        this.changeState('result', answer);
        break;
      case 'result':
        this.changeState('finish', answer);
        break;
      default:
    }
  }
  changeState(newState: string, answer: string) {
    if (newState === 'finish') {
      this.showResult();
    } else {
      this.typing = true;
      setTimeout(() => {
        this.stateMap.items[this.currentState().index].answer = answer;
        this.state = newState;
        this.chats.push({ sender: 0, message: this.currentState().object.message });
        this.typing = false;
        setTimeout(() => { this.scrollToBottom(); }, 100);
        }, 1500);
      this.data.changeStateMap(this.stateMap);
    }
    setTimeout(() => { this.scrollToBottom(); }, 100);
  }
  onSubmitText(answer) {
    console.log(answer);
    this.stateMachine(answer.answer);
  }
  showResult() {
    this.counter = 0;
    this.processing = true;
    setTimeout(() => { this.scrollToBottom(); }, 100);
    const timerId = setInterval(() => {
      this.counter += 1;
      console.log(this.counter);
      setTimeout(() => { this.scrollToBottom(); }, 100);
    }, 2000);
    setTimeout(() => { clearInterval(timerId); }, 10000);
  }
  websiteValidator(url: string) {
    if (url.includes('.')){
      return true;
    }
    else{
      return false;
    }
  }
}
