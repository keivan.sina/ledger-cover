import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormControl} from '@angular/forms';

function duplicatePassword(input: FormControl) {
  if (!input.root || !input.root.get('password')) {
    return null;
  }
  const exactMatch = input.root.get('password').value === input.value;
  return exactMatch ? null : { mismatchedPassword: true };
}

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  users: any;
  registerForm;
  errorMessage = '';
  name = new FormControl('', [
    Validators.required,
  ]);
  password = new FormControl('', [
    Validators.required,
    Validators.minLength(6)
  ]);
  password2 = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    duplicatePassword
  ]);
  email = new FormControl('', [
    Validators.required,
    Validators.email
  ]);
  constructor(
    private formBuilder: FormBuilder,
    public router: Router
  ) {
    this.registerForm = this.formBuilder.group({
      name: this.name,
      email: this.email,
      password: this.password,
      password2: this.password2,
    });
  }

  ngOnInit(): void {
    this.users = JSON.parse(localStorage.getItem('users'));
    if (!JSON.parse(localStorage.getItem('users'))) {
      this.users = [{name: 'K1', email: 'a@b.com', password: '1'}];
      localStorage.setItem('users', JSON.stringify(this.users));
    }
  }

  onSubmit(value) {
    if (this.users.find(x => x.email === value.email)) {
      this.errorMessage = 'This email is already registred. Please Login!';
    } else {
      this.users.push(value);
      localStorage.setItem('users', JSON.stringify(this.users));
      localStorage.setItem('user', JSON.stringify(value));
      console.log(localStorage.getItem(this.users));
      this.router.navigate(['/chat']);
    }
  }
}
