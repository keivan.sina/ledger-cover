export class State {
  public status: string;
  public message: string;
  public inputType: string;
  public options: Array<string>;
  public answer: string;
  constructor(
    status: string,
    message: string,
    inputType: string,
    options: Array<string>,
    answer: string,
    ) {
    this.status = status;
    this.message = message;
    this.inputType = inputType;
    this.options = options;
    this.answer = answer;
  }
  setAnswer(answer) {
    this.answer = answer;
  }
}
