import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  overalRisk = 0;
  risk1 = 0;
  risk2 = 0;
  risk3 = 0;
  risk4 = 0;
  constructor(
    public router: Router
  ) { }

  ngOnInit(): void {
    setTimeout(() => {
      this.overalRisk = 60;
      this.risk1 = 60;
      this.risk2 = 40;
      this.risk3 = 70;
      this.risk4 = 50;
     }, 100);
  }
}
